package crudframework.controller;

import crudframework.controller.common.AbstractController;
import crudframework.entity.RoleEntity;
import crudframework.service.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController extends AbstractController<RoleEntity, RoleService> {
    public RoleController(RoleService service) {
        super(service);
    }

    @Override
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<RoleEntity>> getAll() {
        return super.getAll();
    }
}
