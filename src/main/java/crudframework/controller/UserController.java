package crudframework.controller;

import crudframework.controller.common.AbstractController;
import crudframework.entity.UserEntity;
import crudframework.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController<UserEntity, UserService> {
    public UserController(UserService service) {
        super(service);
    }
}
