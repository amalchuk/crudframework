package crudframework.controller.common;

import crudframework.entity.common.AbstractEntity;
import crudframework.exception.BadRequestException;
import crudframework.exception.NotFoundException;
import crudframework.service.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

public abstract class AbstractController<E extends AbstractEntity, S extends CommonService<E>> implements CommonController<E> {
    protected final S service;

    @Autowired
    public AbstractController(S service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<E> getById(String id) {
        return service.getById(id).map(ResponseEntity::ok).orElseThrow(NotFoundException::new);
    }

    @Override
    public ResponseEntity<List<E>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @Override
    public ResponseEntity<E> save(E entity) {
        return service.save(entity).map(ResponseEntity::ok).orElseThrow(BadRequestException::new);
    }

    @Override
    public ResponseEntity<List<E>> saveAll(List<E> entities) {
        return ResponseEntity.ok(service.saveAll(entities));
    }

    @Override
    public Boolean deleteById(String id) {
        return service.deleteById(id);
    }

    @Override
    public Boolean deleteAll() {
        return service.deleteAll();
    }
}
