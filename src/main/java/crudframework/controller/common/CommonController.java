package crudframework.controller.common;

import crudframework.entity.common.AbstractEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface CommonController<E extends AbstractEntity> {
    @GetMapping
    ResponseEntity<E> getById(@RequestParam("id") String id);

    @GetMapping("/all")
    ResponseEntity<List<E>> getAll();

    @PostMapping
    ResponseEntity<E> save(@RequestBody E entity);

    @PostMapping("/all")
    ResponseEntity<List<E>> saveAll(@RequestBody List<E> entities);

    @DeleteMapping
    Boolean deleteById(@RequestParam("id") String id);

    @DeleteMapping("/all")
    Boolean deleteAll();
}
