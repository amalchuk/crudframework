package crudframework.entity;

import crudframework.entity.common.AbstractEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("roles")
public class RoleEntity extends AbstractEntity {
    @Field("name")
    private String name;

    protected RoleEntity() {
    }

    public RoleEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
