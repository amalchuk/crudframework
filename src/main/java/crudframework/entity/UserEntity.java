package crudframework.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import crudframework.entity.common.AbstractEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Document("users")
public class UserEntity extends AbstractEntity {
    @Field("login")
    private String login;

    @Field("password")
    @JsonProperty(access = WRITE_ONLY)
    private String password;

    @Field("role")
    private RoleEntity role;

    protected UserEntity() {
    }

    public UserEntity(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public UserEntity(String login, String password, RoleEntity role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }
}
