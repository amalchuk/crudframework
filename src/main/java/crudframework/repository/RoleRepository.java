package crudframework.repository;

import crudframework.entity.RoleEntity;
import crudframework.repository.common.CommonRepository;

public interface RoleRepository extends CommonRepository<RoleEntity> {
}
