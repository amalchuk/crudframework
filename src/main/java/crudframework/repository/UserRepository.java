package crudframework.repository;

import crudframework.entity.UserEntity;
import crudframework.repository.common.CommonRepository;

public interface UserRepository extends CommonRepository<UserEntity> {
    UserEntity findByLogin(String login);
}
