package crudframework.repository.common;

import crudframework.entity.common.AbstractEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CommonRepository<E extends AbstractEntity> extends MongoRepository<E, String> {
}
