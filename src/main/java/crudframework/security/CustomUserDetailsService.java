package crudframework.security;

import crudframework.entity.UserEntity;
import crudframework.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity user = userService.get(login).orElseThrow(() -> new UsernameNotFoundException(login));
        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().getName());
        return new User(user.getLogin(), user.getPassword(), Collections.singletonList(authority));
    }
}
