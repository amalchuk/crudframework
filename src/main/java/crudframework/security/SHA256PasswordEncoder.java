package crudframework.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static crudframework.util.Constants.UNICODE;

public class SHA256PasswordEncoder implements PasswordEncoder {
    private static final Logger log = LoggerFactory.getLogger(SHA256PasswordEncoder.class);

    @Override
    public String encode(CharSequence sequence) {
        return encodeString(sequence.toString());
    }

    @Override
    public boolean matches(CharSequence sequence, String s) {
        return MessageDigest.isEqual(encode(sequence).getBytes(UNICODE), s.getBytes(UNICODE));
    }

    private String encodeString(String s) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(s.getBytes(UNICODE), 0, s.length());
            return Base64.getEncoder().encodeToString(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            log.error("SHA-256 message-digest algorithm isn't available", e);
        }

        return null;
    }
}
