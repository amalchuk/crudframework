package crudframework.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.SecureRandom;

import static java.lang.System.currentTimeMillis;

@Configuration
public class SecretKeyConfiguration {
    @Bean
    public byte[] getKey() {
        SecureRandom random = new SecureRandom();
        random.setSeed(currentTimeMillis());

        byte[] secretKey = new byte[255];
        random.nextBytes(secretKey);
        return secretKey;
    }
}
