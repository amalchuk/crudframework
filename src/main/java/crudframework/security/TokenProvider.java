package crudframework.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

import static io.jsonwebtoken.CompressionCodecs.DEFLATE;
import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static java.time.ZoneOffset.UTC;

@Component
public class TokenProvider {
    private static final Logger log = LoggerFactory.getLogger(TokenProvider.class);

    @Autowired
    private SecretKeyConfiguration keyConfiguration;

    public String generate(Authentication authentication) {
        UserDetails details = (UserDetails) authentication.getPrincipal();
        LocalDateTime now = LocalDateTime.now(UTC);

        Claims claims = Jwts.claims()
                .setSubject(details.getUsername())
                .setIssuedAt(Date.from(now.toInstant(UTC)))
                .setExpiration(Date.from(now.plusHours(8).toInstant(UTC)));

        return Jwts.builder()
                .setClaims(claims)
                .compressWith(DEFLATE)
                .signWith(HS256, keyConfiguration.getKey())
                .compact();
    }

    public String getLogin(String accessToken) {
        return Jwts.parser()
                .setSigningKey(keyConfiguration.getKey())
                .parseClaimsJws(accessToken)
                .getBody()
                .getSubject();
    }

    public Boolean validate(String accessToken) {
        try {
            Jwts.parser().setSigningKey(keyConfiguration.getKey()).parseClaimsJws(accessToken);
            return true;
        } catch (JwtException e) {
            log.debug("Failed to parsing or signature validation", e);
        }

        return false;
    }
}
