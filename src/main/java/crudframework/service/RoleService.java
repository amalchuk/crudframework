package crudframework.service;

import crudframework.entity.RoleEntity;
import crudframework.repository.RoleRepository;
import crudframework.service.common.AbstractService;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends AbstractService<RoleEntity, RoleRepository> {
    public RoleService(RoleRepository repository) {
        super(repository);
    }
}
