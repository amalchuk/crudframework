package crudframework.service;

import crudframework.entity.UserEntity;
import crudframework.repository.UserRepository;
import crudframework.service.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService extends AbstractService<UserEntity, UserRepository> {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserService(UserRepository repository) {
        super(repository);
    }

    public Optional<UserEntity> get(String login) {
        return Optional.of(repository.findByLogin(login));
    }

    @Override
    public Optional<UserEntity> save(UserEntity entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        return super.save(entity);
    }

    @Override
    public List<UserEntity> saveAll(List<UserEntity> entities) {
        entities.forEach(entity -> entity.setPassword(passwordEncoder.encode(entity.getPassword())));
        return super.saveAll(entities);
    }
}
