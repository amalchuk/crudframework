package crudframework.service.common;

import crudframework.entity.common.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface CommonService<E extends AbstractEntity> {
    Optional<E> getById(String id);

    List<E> getAll();

    Optional<E> save(E entity);

    List<E> saveAll(List<E> entities);

    Boolean deleteById(String id);

    Boolean deleteAll();
}
