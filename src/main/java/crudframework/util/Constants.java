package crudframework.util;

import java.nio.charset.Charset;

public final class Constants {
    public static final Charset UNICODE = Charset.forName("UTF-8");
}
